/*
 Name:		smartdoorlock.ino
 Created:	18.05.2024 13:46:56
 Author:	Opelutz <WAK-Lab e.V.>

 MIT License

Copyright (c) 2024 Opelutz <WAK-Lab e.V.>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include <ESP8266WiFi.h>
#include <espnow.h>
#include <SHA256.h>
#include "PRIVATE.h"

#define HASH_SIZE 16
#define TIME_REPEAT_REQUEST 200

SHA256 hasher;

volatile bool requestPending = false;

long lastMillis;
int doorStatus;

enum class API_REQUEST
{
	PING,
	OPEN,
	CLOSE,
	CALIBRATE,
	STATUS
};

enum class API_RESPONSE
{
	PONG,
	OPENING,
	CLOSING,
	CALIBRATION,
	OPENED,
	CLOSED,
	UNDEFINED,
	ERROR,
	AUTHENTICATE
};

typedef struct {
	uint8_t API_KEY[16];
	API_REQUEST Value;
}Request;

typedef struct {
	uint8_t NONCE[16];
	API_RESPONSE Value;
}Response;

Request request;
Response response;



void setup() {
	Serial.begin(9600);
	WiFi.mode(WIFI_STA);

	Serial.println(WiFi.macAddress());
	delay(2000);

	uint8_t tempKey;
	uint8_t tempKeyLen;


	if (esp_now_init() != 0) {
		Serial.println("Error on Init");
	}

	esp_now_set_self_role(ESP_NOW_ROLE_COMBO);
	uint8_t result = esp_now_add_peer(receiverAddress, ESP_NOW_ROLE_COMBO, 0, key, 16);

	if (result != 0) {
		Serial.println("Failed to add peer");
	}

	esp_now_register_send_cb(messageSent);
	esp_now_register_recv_cb(messageReceived);
}

void messageSent(uint8_t* macAddr, uint8_t status) {

	Serial.print("Send status: ");
	if (status == 0) {
		Serial.println("Success");
		requestPending = true;
	}
	else {
		Serial.println("Error");
	}
}
void messageReceived(uint8_t* macAddr, uint8_t* data, uint8_t len) {
	memcpy(&response, data, sizeof(response));

	doorStatus = (int)response.Value;
	requestPending = false;

	Serial.print("Response: ");
	switch (response.Value)
	{
	case API_RESPONSE::AUTHENTICATE:
		Serial.println("Authenticate");
		Authenticate(response.NONCE);
		RepeatRequest();
		break;

	case API_RESPONSE::PONG:
		Serial.println("PONG");
		break;

	case API_RESPONSE::OPENING:
		Serial.println("OPENING");
		break;

	case API_RESPONSE::CLOSING:
		Serial.println("CLOSING");
		break;

	case API_RESPONSE::CALIBRATION:
		Serial.println("CALIBRATION");
		break;

	case API_RESPONSE::OPENED:
		Serial.println("OPENED");
		break;

	case API_RESPONSE::CLOSED:
		Serial.println("CLOSED");
		break;

	case API_RESPONSE::UNDEFINED:
		Serial.println("UNDEFINED");
		break;

	case API_RESPONSE::ERROR:
		Serial.println("ERROR");
		break;

	default:
		break;
	}
}

void Authenticate(uint8_t* nonce) {
	uint8_t str[HASH_SIZE];
	for (size_t i = 0; i < sizeof(PRIVATE_API_KEY); i++)
	{
		str[i] = ((long)(PRIVATE_API_KEY[i] * response.NONCE[i])) & 0xFF;
	}
	Hash(str, request.API_KEY, HASH_SIZE);
}

void Hash(const uint8_t* data, uint8_t* buffer, size_t len) {
	hasher.clear();
	hasher.update(data, sizeof(data));
	hasher.finalize(buffer, len);
}

void Open() {
	request.Value = API_REQUEST::OPEN;
	esp_now_send(receiverAddress, (uint8_t*)&request, sizeof(request));
}

void RepeatRequest() {
	esp_now_send(receiverAddress, (uint8_t*)&request, sizeof(request));
}

void loop() {
	if (Serial.available() > 0) {
		char c = Serial.read();

		if (c == 'o') {
			lastMillis = millis();
			request.Value = API_REQUEST::OPEN;
			esp_now_send(receiverAddress, (uint8_t*)&request, sizeof(request));
			requestPending = true;
		}
		if (c == 'c') {
			lastMillis = millis();
			request.Value = API_REQUEST::CLOSE;
			esp_now_send(receiverAddress, (uint8_t*)&request, sizeof(request));
			requestPending = true;
		}
		if (c == 's') {
			lastMillis = millis();
			request.Value = API_REQUEST::STATUS;
			esp_now_send(receiverAddress, (uint8_t*)&request, sizeof(request));
			requestPending = true;
		}
	}

	if (requestPending && (millis() > (lastMillis + TIME_REPEAT_REQUEST))) {
		lastMillis = millis();
		esp_now_send(receiverAddress, (uint8_t*)&request, sizeof(request)); //repeat if no message was received
	}

	return;
}
