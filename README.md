Smart Door Lock via ESP NOW
===========================

# Links

- [WAK-Lab e.V. homepage](https://www.wak-lab.org/)

# Smart door lock

This project consists of a sender and a receiver scetch for an esp8266 to control a selfmade smart door lock.
Our non-profit association "WAK-Lab e.V." found a new space. The problem: many members, less keys.

The idea of a smart lock came to us.

# Contents
- [Receiver](#receiver)
- [Sender](#sender)
- [Documentation](#documentation)
- [License and credits](#license-and-credits)   

# Receiver
## Hardware
The receiver consists of
- esp8266
- battery
- stepper driver
- stepper NEMA 14

## Software
Features:
- [ESP NOW - low power connection](https://github.com/espressif/esp-now)
- encrypted connection
- API for opening | closing | calibrating | status | ...


# Sender
## Hardware
The receiver consists of
- esp8266

## Software
Features:
- [ESP NOW - low power connection](https://github.com/espressif/esp-now)
- encrypted connection


# Documentation


# License and credits

- [ESP NOW](https://github.com/espressif/esp-now?tab=readme-ov-file) included in this build is under Apache License Version 2.0
- [EEPROM](https://github.com/esp8266/Arduino/tree/master) included under GNU LESSER GENERAL PUBLIC LICENSE
- [ESP8266WiFi](https://github.com/esp8266/Arduino/tree/master) included under GNU LESSER GENERAL PUBLIC LICENSE
- [SHA256](https://github.com/rweather/arduinolibs/tree/master) included under MIT License from Southern Storm Software, Pty Ltd.

